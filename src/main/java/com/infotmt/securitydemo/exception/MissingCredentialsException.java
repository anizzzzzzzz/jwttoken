package com.infotmt.securitydemo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.UNAUTHORIZED, reason="Missing Credentials")
public class MissingCredentialsException extends RuntimeException{

}
