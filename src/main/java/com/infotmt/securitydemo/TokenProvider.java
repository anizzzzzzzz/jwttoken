package com.infotmt.securitydemo;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.infotmt.securitydemo.model.Account;
import com.infotmt.securitydemo.repository.AccountRepository;

@Component
public class TokenProvider {

    private final AccountRepository userService;
    private MessageDigest md5er;
    private final String secretKey;


    @Autowired
    public TokenProvider(@Value("${token.secret-key}") String secretKey, AccountRepository userService) {
        try {
            md5er = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Cannot find MD5 algorithm", e);
        }

        if (StringUtils.isEmpty(secretKey)) {
            throw new IllegalArgumentException("Secret key must be set");
        }

        this.secretKey = secretKey;
        this.userService = userService;
    }

    public String getToken(Account user) {
        return getToken(user, DateTime.now().plusDays(1).getMillis());
    }

    public String getToken(Account user, long expirationDateInMillis) {
        return Jwts.builder().setSubject(user.getUsername())
                .claim("roles", user.getRoles())
                .setExpiration(new Date(expirationDateInMillis))
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, "secretKey")
                .compact();
    }

    public Account getUserFromToken(String jwt) {
        Claims claims = Jwts.parser()
                .setSigningKey("secretKey")
                .parseClaimsJws(jwt).getBody();

        return new Account(claims.getSubject(), null, claims.get("roles", String.class), null);

    }

    public void isTokenValid(String encodedToken) {

        Claims claims = Jwts.parser()
                .setSigningKey("secretKey")
                .parseClaimsJws(encodedToken).getBody();
        System.out.println(claims);


    }


}
