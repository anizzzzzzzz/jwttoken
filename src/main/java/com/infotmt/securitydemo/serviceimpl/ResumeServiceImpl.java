package com.infotmt.securitydemo.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.infotmt.securitydemo.model.Resume;
import com.infotmt.securitydemo.repository.ResumeRepository;
import com.infotmt.securitydemo.service.ResumeService;

@Service
public class ResumeServiceImpl implements ResumeService {
	
	@Autowired
	ResumeRepository resumeRepository;

	@Override
	public Page<Resume> findAll(Pageable pageable) {
		return resumeRepository.findAll(pageable);
	}

	@Override
	public Resume findOne(long id) {
		return resumeRepository.findOne(id);
	}

	@Override
	public Resume save(Resume resume) {
		return resumeRepository.save(resume);
	}

	@Override
	public Resume update(Resume resume) {
		return resumeRepository.save(resume);
	}

	@Override
	public void delete(long id) {
		resumeRepository.delete(id);
		
	}

}
