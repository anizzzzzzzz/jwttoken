package com.infotmt.securitydemo.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.infotmt.securitydemo.model.Account;
import com.infotmt.securitydemo.repository.AccountRepository;

@RestController
@RequestMapping("/users")
public class UserController {
    private final AccountRepository accountRepository;

    @Autowired
    public UserController(AccountRepository accountRepository) {

        this.accountRepository = accountRepository;
    }

    @PostMapping
    public Account postAccount(@RequestBody Account input){
            return accountRepository.save(input);
    }


}
