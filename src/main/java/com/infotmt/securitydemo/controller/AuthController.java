package com.infotmt.securitydemo.controller;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import com.infotmt.securitydemo.exception.InvalidCredentialsException;
import com.infotmt.securitydemo.exception.MissingCredentialsException;
import com.infotmt.securitydemo.model.Account;
import com.infotmt.securitydemo.repository.AccountRepository;

import javax.servlet.ServletException;
import java.util.Date;

@RestController
@RequestMapping("/auth")
public class AuthController {
    private final AccountRepository accountRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AuthController(AccountRepository accountRepository, PasswordEncoder passwordEncoder) {
        this.accountRepository = accountRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping(value = "/login")
    public String login(@RequestBody Account input) throws ServletException {
        String username = input.getUsername();
        String password = input.getPassword();

        if (password == null || username == null) throw new MissingCredentialsException();

        Account user;

        if (username.contains("@")) {
            user = accountRepository.findByEmail(username);
        } else {
            user = accountRepository.findByUsername(username);
        }

        if (user == null || !passwordEncoder.matches(password, user.getPassword())) {
            throw new InvalidCredentialsException();
        }

        return Jwts.builder().
                setSubject(user.getUsername())
                .claim("roles", "user")
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, "secretkey").compact();
    }

    @PostMapping("register")
    @ResponseStatus(HttpStatus.CREATED)
    public Account register(@RequestBody Account input) {
        //validateUser(input);

        input.setPassword(passwordEncoder.encode(input.getPassword()));
        return accountRepository.save(input);
    }

}
