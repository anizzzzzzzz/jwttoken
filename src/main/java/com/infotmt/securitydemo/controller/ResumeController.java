package com.infotmt.securitydemo.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.infotmt.securitydemo.model.Resume;
import com.infotmt.securitydemo.service.ResumeService;

@RestController
@RequestMapping("/resume")
public class ResumeController {
	
	@Autowired
	ResumeService resumeService;
	
	@GetMapping
	public ResponseEntity<Page<Resume>> findAll(Pageable pageable){
		Page<Resume> resumes= resumeService.findAll(pageable);
		
		if(!resumes.hasContent())
		{
			return new ResponseEntity<Page<Resume>>(HttpStatus.NO_CONTENT);
		}
		else {
			return new ResponseEntity<Page<Resume>>(resumes,HttpStatus.OK);
		}
		
		
	}
	
	

}
