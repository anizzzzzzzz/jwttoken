package com.infotmt.securitydemo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
public class Education {
	
	@Id
	@GeneratedValue
	private long eid;
	private String instituteName;
	private String degree;
	private String fieldOfStudy;
	
	@ManyToOne
	@JoinColumn(name="rid")
	@JsonIgnore
	private Resume resume;

}
