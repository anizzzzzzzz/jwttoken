package com.infotmt.securitydemo.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
public class Experience {
	
	@Id
	@GeneratedValue
	private long exid;
	private String jobTitle;
	private String company;
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date fromDate;
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date toDate;
	private long exTime;
	
	@ManyToOne
	@JoinColumn(name="rid")
	@JsonIgnore
	private Resume resume;

}
