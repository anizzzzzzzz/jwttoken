package com.infotmt.securitydemo.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

@Entity
@Data
public class Resume {
	
	@Id
	@GeneratedValue
	private long rid;
	private String fname;
	private String lname;
	private String contact;
	private String email;
	private Date dob;
	private String nationality;
	
	@OneToMany(mappedBy="resume")
	List<Education> education;

	@OneToMany(mappedBy="resume")
	List<Experience> experience;
}
