package com.infotmt.securitydemo;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class TokenUtilsImpl implements TokenUtils {
    @Override
    public String getToken(UserDetails userDetails) {
        return null;
    }

    @Override
    public String getToken(UserDetails userDetails, Long expiration) {
        return null;
    }

    @Override
    public boolean validate(String token) {
        return false;
    }

    @Override
    public UserDetails getUserFromToken(String token) {
        return null;
    }
}
