package com.infotmt.securitydemo.service;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

import com.infotmt.securitydemo.model.Account;


public class TokenAuthenticationService {

    private final String SECRET = "secretkey";
//    static final String TOKEN_PREFIX = "Bearer";
//    static final String HEADER_STRING = "Authorization";

    public String getToken(Account account) {
        return Jwts.builder()
                .setSubject(account.getUsername())
                .claim("roles", account.getRoles())
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, SECRET)
                .compact();
    }

    public String parseToken(String token){
        Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody();
        return "";
    }
}
