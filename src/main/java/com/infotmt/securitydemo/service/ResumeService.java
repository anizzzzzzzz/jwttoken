package com.infotmt.securitydemo.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.infotmt.securitydemo.model.Resume;

public interface ResumeService {
	
	Page<Resume> findAll(Pageable pageable);
	
	Resume findOne(long id);
	
	Resume save(Resume resume);
	
	Resume update(Resume resume);
	
	void delete(long id);

}
