package com.infotmt.securitydemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.infotmt.securitydemo.model.Resume;

public interface ResumeRepository extends JpaRepository<Resume, Long> {
	
	

}
