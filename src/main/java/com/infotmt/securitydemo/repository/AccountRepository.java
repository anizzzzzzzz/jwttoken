package com.infotmt.securitydemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.infotmt.securitydemo.model.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {
    Account findByUsername(String username);
    Account findByEmail(String email);
}