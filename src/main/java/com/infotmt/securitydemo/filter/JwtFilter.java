package com.infotmt.securitydemo.filter;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.filter.GenericFilterBean;

import com.infotmt.securitydemo.exception.InvalidTokenException;
import com.infotmt.securitydemo.exception.MissingOrInvalidAuthorizationHeaderException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.util.Collections.emptyList;

public class JwtFilter extends GenericFilterBean {

    private final AuthenticationManager authenticationManager;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    public JwtFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        final HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        final HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        final String authHeader = httpServletRequest.getHeader("authorization");
        logger.info(authHeader);
        if ("OPTIONS".equals(httpServletRequest.getMethod())) {
            httpServletResponse.setStatus(HttpServletResponse.SC_OK);
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            throw new MissingOrInvalidAuthorizationHeaderException();
        }

        final String token = authHeader.substring(7);

        try {
            final Claims claims = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody();
            logger.info(claims.getSubject());
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
                    = new UsernamePasswordAuthenticationToken(claims.getSubject(), null, emptyList());

            usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetails(httpServletRequest));
            
            
            Authentication authentication = this.authenticationManager.authenticate(usernamePasswordAuthenticationToken);
            
            
            SecurityContextHolder.getContext().setAuthentication(authentication);
            httpServletRequest.setAttribute("claims", claims);
        } catch (final SignatureException e) {
            throw new InvalidTokenException();
        }

        filterChain.doFilter(servletRequest, servletResponse);

    }

}
